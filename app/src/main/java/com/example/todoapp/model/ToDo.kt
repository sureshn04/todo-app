package com.example.todoapp.model

import java.io.Serializable

class ToDo(var name :String) : Serializable {
    var id : Int? = null;
    var task : String? = null;
    var description : String? = null;
    var timestamp : String? = null;

    constructor(id: Int, task: String, description: String, timestamp:String) : this(task){
        this.id = id
        this.task = task
        this.description = description
        this.timestamp = timestamp
    }

    constructor(task: String, description: String):this(task){
        this.task= task
        this.description = description
    }

}