package com.example.todoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.todoapp.adapter.MyAdapter
import com.example.todoapp.helper.DBHelper
import com.example.todoapp.model.ToDo
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MyAdapter.onClickListener {

    private var todoList = ArrayList<ToDo>()
    private lateinit var myAdapter : MyAdapter
    private lateinit var dbHelper: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


    }

    override fun onItemClick(todo: ToDo, position: Int) {
        TODO("Not yet implemented")
    }

    override fun onItemDelete(todo: ToDo) {
        TODO("Not yet implemented")
    }

}
