package com.example.todoapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.model.ToDo
import kotlinx.android.synthetic.main.list_item.view.*

class MyAdapter (val todoList: Array<ToDo>): RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    private var listener: onClickListener? = null;

    fun setListener(clickListener: onClickListener){
        this.listener = clickListener
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bindItems(todo : ToDo){
            val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
            val tvDesc = itemView.findViewById<TextView>(R.id.tvDesc)
            val tvTimeStamp = itemView.findViewById<TextView>(R.id.tvTimeStamp)

            tvTitle.text = todo.task
            tvDesc.text = todo.description
            tvTimeStamp.text = todo.timestamp
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return  todoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val todo = todoList[position]
        holder.bindItems(todo)

        holder.itemView.setOnClickListener(View.OnClickListener {
            listener?.onItemClick(todo, position);
        })

        holder.itemView.btnDelete.setOnClickListener(View.OnClickListener {
            listener?.onItemDelete(todo)
        })
    }

    interface onClickListener{
        fun onItemClick(todo: ToDo, position: Int)
        fun onItemDelete(todo: ToDo)
    }

}