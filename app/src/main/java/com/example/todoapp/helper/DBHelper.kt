package com.example.todoapp.helper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import androidx.core.content.contentValuesOf
import com.example.todoapp.model.ToDo

class DBHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION)
{
    companion object{
        const val DB_NAME = "TodoApp"
        const val DB_VERSION = 1
    }

    private val DB_TABLE = "todo"
    private val COLUMN_ID = "id"
    private val COLUMN_TASK = "task"
    private val COLUMN_DESSC = "description"
    private val COLUMN_TIMESTAMP = "timestamp"

    private val TABLE = ("create table $DB_TABLE($COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, $COLUMN_TASK TEXT, $COLUMN_DESSC TEXT, $COLUMN_TIMESTAMP DATETIME DEFAULT CURRENT_TIMESTAMP);")

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL(TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $DB_TABLE")
        onCreate(db)
    }

    val allToDos : ArrayList<ToDo>
    get() {
        val todos = ArrayList<ToDo>()
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $DB_TABLE ORDER BY $COLUMN_TIMESTAMP DESC", null)
        if(cursor.moveToFirst()){
            do {
                val todo = ToDo(cursor!!.getInt(cursor.getColumnIndex(COLUMN_ID)), cursor.getString(cursor.getColumnIndex(COLUMN_TASK)), cursor.getString(cursor.getColumnIndex(COLUMN_DESSC)), cursor.getString(cursor.getColumnIndex(COLUMN_TIMESTAMP)))

                todos.add(todo)
            } while (cursor.moveToFirst())
        }

        db.close()
        return todos
    }


    fun insertToDO(toDo: ToDo): Long{
        val values = ContentValues()
        val db = this.writableDatabase

        values.put(COLUMN_TASK, toDo.task)
        values.put(COLUMN_DESSC, toDo.description)

        val id = db.insert(DB_TABLE,null, values)
        db.close()
        return id
    }

    fun getToDo(id: Long) : ToDo{
        val db = this.readableDatabase
        val cursor = db.query(DB_TABLE, arrayOf(COLUMN_ID, COLUMN_TASK, COLUMN_DESSC, COLUMN_DESSC), "$COLUMN_ID =? ${arrayOf(id.toShort())}", null, null, null,null)

        cursor?.moveToFirst()
        val toDo = ToDo(
                cursor!!.getInt(cursor.getColumnIndex(COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TASK)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESSC)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TIMESTAMP)))

        db.close()
        return toDo
    }

    fun updateToDo(toDo: ToDo): Int{
        val values = ContentValues()
        val db = this.writableDatabase

        values.put(COLUMN_TASK, toDo.task)
        values.put(COLUMN_DESSC, toDo.description)

        return db.update(DB_TABLE, values, "$COLUMN_ID =? ", arrayOf(toDo.id.toString()))
    }

    fun deleteToDo(toDo: ToDo) : Boolean{
        this.writableDatabase.delete(DB_TABLE, "$COLUMN_ID =?", arrayOf(toDo.id.toString()))
        return true
    }
}